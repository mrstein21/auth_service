const userReposotry = require('../repository/auth_repository');
var bcrypt=require("bcryptjs");
var dotenv=require("dotenv");
var axios=require("axios");
dotenv.config();
module.exports={
    login_use_case:async(request)=>{
        var data= await userReposotry.login(request.email,request.password);
        if(data==null){
          return { 
             "success":false,
             "message":"Username atau Password salah",
          }
        }else{
          var response=await axios.post(process.env.KONG_ADMIN+"/consumers/"+process.env.CONSUMER+"/key-auth");
          return {
             "success":true,
             "data":data,
             "apikey":response.data.key
          };
        }
    },
    register_use_case:async(request)=>{
        var user={
            email:request.email,
            name:request.name,
            password: bcrypt.hashSync(request.password,8),
         }
          var data= await userReposotry.register(user);
          if(data==null){
            return {
               "success":false,
               "message":"User sudah terdaftar",
            };
          }
          var response=await axios.post(process.env.KONG_ADMIN+"/consumers/"+process.env.CONSUMER+"/key-auth");
          return {
              "success":true,
              "apikey":response.data.key,
              "data":data,
          }
    }
}